// Generated automatically by Perfect Assistant Application
// Date: 2017-11-10 08:09:05 +0000
import PackageDescription
let package = Package(
	name: "WebSocket",
	targets: [],
	dependencies: [
		.Package(url: "https://github.com/PerfectlySoft/Perfect-HTTPServer.git", majorVersion: 3),
		.Package(url: "https://github.com/PerfectlySoft/Perfect-WebSockets.git", majorVersion: 3),
	]
)
